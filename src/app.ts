import { Preload } from "./Scenes/Preload";
import { Boot } from "./Scenes/Boot";
import { Game } from "./Scenes/Game";


const config:GameConfig = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    scene: [
        Preload,
        Boot,
        Game
    ]
};

const game = new Phaser.Game(config);