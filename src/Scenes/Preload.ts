export class Preload extends Phaser.Scene {
    constructor() {
        super({
            key: 'preloader'
        });
        console.log('constructor');
    }

    public preload(): void {
        this.load.image('bg-static', 'assets/square.png')
        this.load.image('bg-overlay', 'assets/bg.png')
    }
}